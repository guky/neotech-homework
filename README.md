###Run application:
_./gradlew bootRun_

###Example request:
####POST: _http://localhost:8080/phone/country-code_
Request: _{"phoneNumber" : "+3718882848"}_  
Response: _{"codes":[{"code":"LV","countryName":"Latvia"}]}_


###UI
_http://localhost:8080/index.html_
