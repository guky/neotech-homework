package my.homework.neotech.infrastructure;

public class Checks {
    public static void checkArgument(boolean expression, String message, Object... params) {
        if (!expression) {
            throw new IllegalArgumentException(String.format(message, params));
        }
    }
}
