package my.homework.neotech.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Configuration
public class Config {

    @Bean
    public MappingJackson2HttpMessageConverter httpMessageConverter() {
        return new MappingJackson2HttpMessageConverter(JSON.mapper);
    }
}
