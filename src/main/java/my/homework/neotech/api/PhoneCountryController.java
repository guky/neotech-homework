package my.homework.neotech.api;

import my.homework.neotech.api.PhoneCountryRequest.Response.PhoneCountries;
import my.homework.neotech.phone.ValidPhoneNumber;
import my.homework.neotech.phone.resolve.PhoneCountryFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/phone")
public class PhoneCountryController {

    private final PhoneCountryFactory countryFactory;

    public PhoneCountryController(PhoneCountryFactory countryFactory) {
        this.countryFactory = countryFactory;
    }

    @PostMapping("/country-code")
    public PhoneCountries lookupCountry(@RequestBody PhoneCountryRequest request) {
        ValidPhoneNumber validPhoneNumber = new ValidPhoneNumber(request.phoneNumber);
        return countryFactory.create(validPhoneNumber);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Error> handleException(IllegalArgumentException e) {
        return ResponseEntity.badRequest()
                .body(new Error(e.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Error> handleException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new Error(e.getMessage()));
    }

}
