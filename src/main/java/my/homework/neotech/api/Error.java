package my.homework.neotech.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
    public final String message;

    @JsonCreator
    public Error(@JsonProperty("message") String message) {
        this.message = message;
    }
}
