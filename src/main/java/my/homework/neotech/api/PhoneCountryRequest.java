package my.homework.neotech.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vavr.collection.Set;

import java.util.*;

import static java.util.Arrays.asList;

public class PhoneCountryRequest {
    private final static List<String> isoCountries = asList(Locale.getISOCountries());

    public final String phoneNumber;

    @JsonCreator
    public PhoneCountryRequest(@JsonProperty("phoneNumber") String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public interface Response {

        class PhoneCountries {
            public final java.util.Set<PhoneCountry> codes;


            public PhoneCountries(Set<PhoneCountry> codes) {
                this.codes = codes.toJavaSet();
            }

            @JsonCreator
            public PhoneCountries(@JsonProperty("codes") HashSet<PhoneCountry> codes) {
                this.codes = new HashSet<>(codes);
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                PhoneCountries that = (PhoneCountries) o;
                return codes.equals(that.codes);
            }

            @Override
            public int hashCode() {
                return Objects.hash(codes);
            }

            @Override
            public String toString() {
                return "PhoneCountries{" +
                        "codes=" + codes +
                        '}';
            }
        }

        class PhoneCountry {

            public final String code;
            public final String countryName;


            @JsonCreator
            private PhoneCountry(@JsonProperty("code") String code, @JsonProperty("countryName") String countryName) {
                this.code = code;
                this.countryName = countryName;
            }

            public PhoneCountry(String code) {
                this.code = code.toUpperCase();

                if (isValidCode()) {
                    this.countryName = new Locale("", this.code).getDisplayCountry();
                } else {
                    this.countryName = "unknown";
                }
            }

            private boolean isValidCode() {
                return isoCountries.contains(this.code);
            }

            @Override
            public String toString() {
                return "PhoneCountry{" +
                        "code='" + code + '\'' +
                        ", countryName='" + countryName + '\'' +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                PhoneCountry that = (PhoneCountry) o;
                return code.equals(that.code);
            }

            @Override
            public int hashCode() {
                return Objects.hash(code);
            }
        }
    }
}
