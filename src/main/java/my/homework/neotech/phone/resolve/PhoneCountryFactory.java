package my.homework.neotech.phone.resolve;

import my.homework.neotech.api.PhoneCountryRequest.Response.PhoneCountries;
import my.homework.neotech.phone.ValidPhoneNumber;

public interface PhoneCountryFactory {
    PhoneCountries create(ValidPhoneNumber phoneNumber);
}
