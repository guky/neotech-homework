package my.homework.neotech.phone.resolve;

import io.vavr.collection.*;
import io.vavr.control.Option.Some;
import my.homework.neotech.api.PhoneCountryRequest.Response.PhoneCountries;
import my.homework.neotech.api.PhoneCountryRequest.Response.PhoneCountry;
import my.homework.neotech.phone.ValidPhoneNumber;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.function.Function;
import java.util.function.Predicate;

import static io.vavr.collection.List.ofAll;
import static io.vavr.control.Option.of;
import static java.lang.Integer.compare;
import static java.lang.Integer.parseInt;

@Component
public class WikiPhoneCountryFactory implements PhoneCountryFactory {
    private static final String phoneCountryCode = "[0-9]{1,2}x";
    private final SortedSet<PhoneCodeCountries> phoneCodeCountries;

    public WikiPhoneCountryFactory(@Value("${country.code.lookup.table.url}") String codeTableUrl) {
        try {
            Document document = Jsoup.connect(codeTableUrl).get();
            Element countryCodeTable = countryCodeTable(document);

            phoneCodeCountries = ofAll(countryCodeTable.getElementsByTag("tr").stream())
                    .filter(tr -> isPhoneCountryRow(tr) || isAmericanCountryRow(tr))
                    .map(PhoneCodeRow::new)
                    .flatMap(row -> row.phoneCodeToCountryCodes.toList())
                    .map(tuple -> new PhoneCodeCountries(tuple._1, tuple._2))
                    .toSortedSet();

        } catch (IOException e) {
            throw new IllegalStateException("Failed to fetch Country code table from wiki [" + codeTableUrl + "] ", e);
        }
    }

    public PhoneCountries create(ValidPhoneNumber phoneNumber) {
        Set<PhoneCountry> countries = phoneCodeCountries
                .filter(it -> phoneNumber.numberPart().startsWith(it.code))
                .take(1)
                .flatMap(phoneCode -> phoneCode.countries)
                .toLinkedSet()
                .map(PhoneCountry::new);

        return new PhoneCountries(countries);
    }

    private Boolean isPhoneCountryRow(Element tr) {
        return of(tr.getElementsByTag("th"))
                .filter(th -> th.text().matches(phoneCountryCode))
                instanceof Some;
    }

    private Boolean isAmericanCountryRow(Element tr) {
        return tr.getElementsByTag("th").isEmpty();
    }

    private Element countryCodeTable(Document document) {
        return document.getElementsByClass("wikitable").first();
    }


    private static class PhoneCodeRow {
        private final Map<String, List<String>> phoneCodeToCountryCodes;

        private PhoneCodeRow(Element tr) {
            phoneCodeToCountryCodes = ofAll(tr.getElementsByTag("td").stream())
                    .filter(this::isCountryCodeCell)
                    .map(td -> phoneCodeToCountryCodes(td)
                            .filter(notEmptyPhoneCodeToCountryCodes())
                            .map(asList())
                            .toMap(phoneCodeWithoutCountries(), countryCodesWithoutPhoneCode()))
                    .fold(HashMap.empty(), Map::merge);
        }

        private Function<List<String>, List<String>> countryCodesWithoutPhoneCode() {
            return List::pop;
        }

        private Function<List<String>, String> phoneCodeWithoutCountries() {
            return List::peek;
        }

        private Function<String, List<String>> asList() {
            return phoneCodeToCountryCodes -> List.of(phoneCodeToCountryCodes.split(":"));
        }

        private Predicate<String> notEmptyPhoneCodeToCountryCodes() {
            return it -> !it.equals(":");
        }

        private List<String> phoneCodeToCountryCodes(Element td) {
            return List.of(stripTags(td).split("\\+"));
        }

        private String stripTags(Element td) {
            if (td.childrenSize() == 0) {
                return td.text();
            }
            return ofAll(td.children().stream())
                    .map(Element::text)
                    .map(it -> it.replaceAll("\\s+", ""))
                    .fold("", (all, current) -> all + ":" + current);
        }

        private boolean isCountryCodeCell(Element td) {
            return td.text().contains(":") && td.getElementsByTag("a").size() > 1;
        }
    }

    private static class PhoneCodeCountries implements Comparable<PhoneCodeCountries> {
        public final String code;
        public final List<String> countries;
        private final int codeNumber;

        private PhoneCodeCountries(String code, List<String> countries) {
            this.code = code;
            this.codeNumber = parseInt(code);
            this.countries = countries;
        }

        @Override
        public String toString() {
            return "PhoneCodeCountries{" +
                    "code='" + code + '\'' +
                    ", countries=" + countries +
                    '}';
        }

        @Override
        public int compareTo(PhoneCodeCountries o) {
            return compare(o.codeNumber, this.codeNumber);
        }
    }
}
