package my.homework.neotech.phone;

import java.util.regex.Pattern;

import static my.homework.neotech.infrastructure.Checks.checkArgument;

public class ValidPhoneNumber {
    private static final int maxLen = 16;
    private static final Pattern pattern = Pattern.compile("[+][0-9]{1,15}");

    private final String phone;

    public ValidPhoneNumber(String phone) {
        checkArgument(phone != null && !phone.isEmpty(), "Phone number required");
        checkArgument(phone.length() <= maxLen, "Maximum allowed phone length [%s] but was [%s]", maxLen, phone.length());
        checkArgument(pattern.matcher(phone).matches(), "Incorrect phone number [%s]. Should be '+' and [%s] digits", phone, maxLen - 1);
        this.phone = phone;
    }

    public String numberPart() {
        return phone.replace("+", "");
    }

    @Override
    public String toString() {
        return phone;
    }
}
