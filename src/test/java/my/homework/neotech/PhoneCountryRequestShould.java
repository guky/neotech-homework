package my.homework.neotech;

import com.fasterxml.jackson.core.JsonProcessingException;
import my.homework.neotech.api.Error;
import my.homework.neotech.api.PhoneCountryRequest;
import my.homework.neotech.api.PhoneCountryRequest.Response.PhoneCountries;
import my.homework.neotech.api.PhoneCountryRequest.Response.PhoneCountry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.util.stream.Stream;

import static io.vavr.collection.HashSet.of;
import static my.homework.neotech.infrastructure.JSON.mapper;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class PhoneCountryRequestShould {


    public static final String PHONE_LOOKUP = "/phone/country-code";
    @LocalServerPort
    int randomServerPort;
    @Autowired
    MappingJackson2HttpMessageConverter messageConverter;
    RestTemplate rest;

    private static Stream<Arguments> resolveCountryCode() {
        return Stream.of(
                Arguments.of("+37126788617", new PhoneCountries(of(new PhoneCountry("LV"))), "Happy path"),
                Arguments.of("+737126788617", new PhoneCountries(of(new PhoneCountry("RU"))), "Exactly matching RU for +73"),
                Arguments.of("+717126788617", new PhoneCountries(of(new PhoneCountry("RU"), new PhoneCountry("KZ"))), "+7 matches both KZ and RU"),
                Arguments.of("+217126788617", new PhoneCountries(of()), "Empty set on un existing phone country code")
        );
    }

    @BeforeEach
    void setUp() {
        rest = new RestTemplate();
        rest.getMessageConverters().add(messageConverter);
        rest.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:" + randomServerPort));
    }

    @ParameterizedTest(name = "{0} => {2}")
    @MethodSource
    void resolveCountryCode(String phone, PhoneCountries expectedCountries, String description) {
        PhoneCountries countries = rest.postForObject(PHONE_LOOKUP, new PhoneCountryRequest(phone), PhoneCountries.class);

        assertEquals(expectedCountries, countries);
    }

    @Test
    void returnBadRequestOnIncorrectInput() throws JsonProcessingException {
        BadRequest error = assertThrows(BadRequest.class, () ->
                rest.postForObject(PHONE_LOOKUP, new PhoneCountryRequest("abc"), PhoneCountry.class));

        Error apiError = mapper.readValue(error.getResponseBodyAsString(), Error.class);
        assertTrue(apiError.message.contains("Incorrect phone"));
    }
}
