package my.homework.neotech.phone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidPhoneNumberShould {
    String validPhoneString = "+15232567424";

    @Test
    public void createValidPhoneNumberFromValidInput() {
        ValidPhoneNumber phoneNumber = new ValidPhoneNumber(validPhoneString);
        assertEquals(validPhoneString, phoneNumber.toString());
    }

    @Test
    public void throwOnTooLongPhoneNumber() {
        String longPhoneString = "+444445654556554646666644";
        IllegalArgumentException error = assertThrows(IllegalArgumentException.class, () -> {
            new ValidPhoneNumber(longPhoneString);
        });

        assertTrue(error.getMessage().contains("Maximum allowed"), "Error message should contain 'Maximum allowed'");
    }

    @Test
    public void throwOnNullPhoneNumber() {
        IllegalArgumentException error = assertThrows(IllegalArgumentException.class, () -> {
            new ValidPhoneNumber(null);
        });

        assertTrue(error.getMessage().contains("required"), "Error message should contain 'required'");
    }

    @Test
    public void throwOnEmptyPhoneNumber() {
        IllegalArgumentException error = assertThrows(IllegalArgumentException.class, () -> {
            new ValidPhoneNumber("");
        });

        assertTrue(error.getMessage().contains("required"), "Error message should contain 'required'");
    }

    @Test
    public void throwOnPhoneNumberWithAlphaNumeric() {
        String alphaNumericPhoneString = "+4a4445654556554";
        IllegalArgumentException error = assertThrows(IllegalArgumentException.class, () -> {
            new ValidPhoneNumber(alphaNumericPhoneString);
        });

        assertTrue(error.getMessage().contains("Incorrect phone"), "Error message should contain 'Incorrect phone'");
    }

    @Test
    public void throwOnPhoneNumberMissingPlusSign() {
        String withoutPlusPhoneString = validPhoneString.replace("+", "");
        IllegalArgumentException error = assertThrows(IllegalArgumentException.class, () -> {
            new ValidPhoneNumber(withoutPlusPhoneString);
        });

        assertTrue(error.getMessage().contains("Incorrect phone"), "Error message should contain 'Incorrect phone'");
    }
}
